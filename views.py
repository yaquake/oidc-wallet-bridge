from flask import Blueprint, request
import hashlib
import hmac
import json
from base64 import b64encode
from uuid import uuid4

import requests
from authlib.oauth2 import OAuth2Error
from flask import Blueprint, request
from flask import redirect, jsonify

import app
from dia_fab_model import authorization
from models import User

bp = Blueprint(__name__, 'dia-fab')


def get_oidc_token(scope, state, platform, bad_nonce=None, redirect_uri=None):
    if platform == 'oti2':
        oti_url = app.app.config['OTI2_URL']
        shared_key = 'shared_wallet_oti.key'
    elif platform == 'oti1':
        oti_url = app.app.config['OTI1_URL']
        shared_key = 'shared_wallet_oti.key'
    elif platform == 'uat':
        oti_url = app.app.config['UAT_URL']
        shared_key = 'shared_wallet_uat.key'
    elif platform == 'prod':
        oti_url = app.app.config['PROD_URL']
        shared_key = 'shared_wallet_prod.key'
    
    DAON_id = str(uuid4())
    # from azure.keyvault.secrets import SecretClient
    # from azure.identity import DefaultAzureCredential
    # KVUri = 'https://kv-fab-common.vault.azure.net/'
    # credential = DefaultAzureCredential()
    # client = SecretClient(vault_url=KVUri, credential=credential)
    # key = client.get_secret('private-key')
    with open(shared_key, 'rb') as f:
        key = f.read()
    signature = b64encode(hmac.new(key, DAON_id.encode('utf-8'), hashlib.sha256).digest())

    if bad_nonce:
        signature = b"bad_nonce"

    if redirect_uri:
        redirect_uri_final = redirect_uri
    else:
        redirect_uri_final = 'http://ec2-13-236-59-222.ap-southeast-2.compute.amazonaws.com/redirect/'

    params = {
        'client_id': 'wallet/dia.govt.nz',
        'response_type': 'id_token',
        'redirect_uri': redirect_uri_final,
        'scope': scope,
        'nonce': signature,
        'login_hint': DAON_id,
        'state': state,
        'platform': platform,
    }


    response = requests.get(
        oti_url + '/oauth/authorize',
        params=params,
        allow_redirects=False,
        verify=False,
    )
    # print('*'*100)
    # print(response.text)
    return response.headers['Location']


@bp.route('/oauth/authorize', methods=['GET', 'POST'])
def authorize():
    user = User.query.get(1)
    if request.method == 'GET':
        try:
            grant = authorization.validate_consent_request(end_user=user)
        except OAuth2Error as error:
            import traceback
            traceback.print_exc()
            return jsonify(dict(error.get_body()))
        state = str(uuid4())

        scope = "openid liveness passport photo_fr"
        platform = request.args.get('platform', 'oti2')
        bad_nonce = None
        redirect_uri = 'https://app-oidc.azurewebsites.net'

        bridge_url = get_oidc_token(scope, state, platform, bad_nonce, redirect_uri)
        redirect_url_final = bridge_url + "&nonce=" + request.args['nonce'] + "&access_token=" + request.args['access_token']
        return redirect(redirect_url_final, code=302)

    r = authorization.create_authorization_response(grant_user=user)
    redirect_url_final = r.location + "&credential_code=" + request.form['code']
    return redirect(redirect_url_final, code=302)


@bp.route('/oauth/token', methods=['POST'])
def issue_token():
    credential_code = request.form['credential_code']
    params = '?platform=' + request.form['platform']
    if request.form['platform'] == 'oti2':
        url = app.app.config['CREDENTIAL_URL_2']
    elif request.form['platform'] == 'oti1':
        url = app.app.config['CREDENTIAL_URL_1']
    elif request.form['platform'] == 'uat':
        url = app.app.config['CREDENTIAL_URL_UAT']
    elif request.form['platform'] == 'prod':
        url = app.app.config['CREDENTIAL_URL_PROD']
    try:
        res_credential_oti = requests.get(url, headers={'Content-type': 'application/json', "Authorization": "Bearer " + credential_code}, verify=False, )
        print(res_credential_oti)
        identity_credential = res_credential_oti.json()['identity_credential']
        photo_credential = res_credential_oti.json()['photo_credential']
    except Exception as e:
        identity_credential = ""
        photo_credential = ""
        print("error happened when fetch the credentials from the OTI server!")
        print(e)

    r = authorization.create_token_response()
    if r.status_code == 200:

        res_final = {}
        res_final['access_token'] = json.loads(r.response[0].decode('utf-8'))['access_token']
        res_final['expires_in'] = json.loads(r.response[0].decode('utf-8'))['expires_in']
        res_final['scope'] = "openid credential"
        res_final['token_type'] = json.loads(r.response[0].decode('utf-8'))['token_type']
        res_final['identity_credential'] = identity_credential
        res_final['photo_credential'] = photo_credential
        return jsonify(res_final)
    else:
        res = {"error": "invalid request"}
        return res

@bp.route('/', methods=['GET'])
def get_error():
    try:
        error = request.args.get('error', None)
        return redirect(f'idapp://?error={error}')
    except:
        pass