FROM python:3.6

ENV PYTHONUNBUFFERED 1

ARG uid=1000

RUN apt-get update && apt-get upgrade -y && apt-get install -y --no-install-recommends vim default-mysql-client libxml2-dev libxmlsec1-dev ffmpeg && rm -rf /var/lib/apt/lists/*

COPY . /code
WORKDIR /code
RUN pip install -r requirements.txt
