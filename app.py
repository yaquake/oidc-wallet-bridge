# from dia_wallet_bridge.app import create_app
from flask import Flask

from dia_fab_model import config_oauth
from models import db


def initalize_app():
    app = Flask(__name__)
    app.config.from_object('settings')

    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {"pool_pre_ping": True}
    db.init_app(app)
    config_oauth(app)
    from views import bp
    app.register_blueprint(bp, url_prefix='')
    return app


app = initalize_app()

