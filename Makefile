.PHONY: clean total-clean image up down test coverage

clean:
	find . -name "*.pyc" -delete

total-clean: clean
	find . -name "*~" -delete
	find . -name "*.swp"  -delete

image: total-clean
	docker build -t diaoti.azurecr.io/dia_wallet_bridge -f Dockerfile .

up:
	docker-compose -f docker-compose.yml up -d --force-recreate

down:
	docker-compose -f docker-compose.yml down -v

test: clean
	flask test

coverage: clean
	coverage run --source='.' --branch --omit="settings.py" `which flask` test
	coverage report

attach-to-app:
	docker stop docker_app_1
	docker-compose -f docker/docker-compose.yml run --service-ports app
