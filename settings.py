# coding: utf-8
import os
import sys
from logging.config import dictConfig


module = sys.modules[__name__]


def set_from_env(var, default, conv=None, verbose=False):
    value = os.environ.get(var, default)
    if conv:
        value = conv(value)
    if verbose:
        print('{} = {}'.format(var, value))
    setattr(module, var, value)


def str2bool(s):
    if isinstance(s, bool):
        return s
    return s.lower() in ['true', '1', 't', 'y', 'yes', 'on', 'positive', 'on', 'enable']


OAUTH2_JWT_ENABLED = True

OAUTH2_JWT_ISS = 'https://www.fab.govt.nz'
OAUTH2_JWT_KEY = 'CM60S1DFJNGYAVIJI8D9VC5CQLMIC0CVXEJA4OLNVH22B0GND4'
OAUTH2_JWT_ALG = 'RS256'

set_from_env('DEBUG', True, str2bool)
set_from_env('XDEBUG', False, str2bool)
set_from_env('LOGGER_NAME', 'daon-api-wrapper')
set_from_env('LOG_FILE', './catchall.log')
set_from_env('ERROR_FILE', './daon-api-wrapper.log')
set_from_env('LOG_LEVEL', 'DEBUG')
set_from_env('AUDIT_FILE', './audit.log')
set_from_env('SQLALCHEMY_TRACK_MODIFICATIONS', False, str2bool)
set_from_env('SQLALCHEMY_DATABASE_URI', 'postgres://faboidcadmin@psql-fab-oidc:abcd-1234@psql-fab-oidc.postgres.database.azure.com:5432/postgres')
set_from_env('OTI2_URL', 'https://oti2.australiaeast.cloudapp.azure.com/api')
set_from_env('CREDENTIAL_URL_2', 'https://oti2.australiaeast.cloudapp.azure.com/api/share-consent')
set_from_env('OTI1_URL', 'https://tam-liveness.australiaeast.cloudapp.azure.com/api')
set_from_env('CREDENTIAL_URL_1', 'https://tam-liveness.australiaeast.cloudapp.azure.com/api/share-consent')
set_from_env('UAT_URL', 'https://test.oti.dia.govt.nz/api')
set_from_env('CREDENTIAL_URL_UAT', 'https://test.oti.dia.govt.nz/api/share-consent')
set_from_env('PROD_URL', 'https://oti.dia.govt.nz/api')
set_from_env('CREDENTIAL_URL_PROD', 'https://oti.dia.govt.nz/api/share-consent')


# Configure Logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '%(asctime)s %(levelname)s %(name)s %(filename)s:%(lineno)d %(message)s'
        }
    },
    'handlers': {
        'catchall': {
            'level': 'DEBUG',
            'formatter': 'simple',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': module.LOG_FILE,
            'maxBytes': 1 * 1024 * 1024,  # 1MB
            'backupCount': 10,
            'encoding': 'utf-8',
        },
        'error': {
            'level': 'DEBUG',
            'formatter': 'simple',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': module.ERROR_FILE,
            'maxBytes': 1 * 1024 * 1024,  # 1MB
            'backupCount': 10,
            'encoding': 'utf-8',
        },
        'audit': {
            'level': 'INFO',
            'formatter': 'simple',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': module.AUDIT_FILE,
            'maxBytes': 1 * 1024 * 1024,  # 1MB
            'backupCount': 10,
            'encoding': 'utf-8',
        },
    },
    'loggers': {
        '': {
            'handlers': ['catchall'],
            'level': module.LOG_LEVEL,
            'propagate': True,
        },
        'flask.app': {
            'handlers': ['error'],
            'level': module.LOG_LEVEL,
            'propagate': False,
        },
        'audit': {
            'handlers': ['audit'],
            'level': 'INFO',
            'propagate': False,
        },
    },
}

dictConfig(LOGGING)
